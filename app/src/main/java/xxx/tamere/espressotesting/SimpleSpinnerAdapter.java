package xxx.tamere.espressotesting;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SimpleSpinnerAdapter extends BaseAdapter {
    private Context context;
    private String[] items = { "eat", "drink" };

    public SimpleSpinnerAdapter (Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView currentView;

        if(convertView != null) {
            currentView = (TextView)convertView;
        } else {
            currentView = new TextView(context);
        }

        currentView.setText(items[position]);

        return currentView;
    }
}
