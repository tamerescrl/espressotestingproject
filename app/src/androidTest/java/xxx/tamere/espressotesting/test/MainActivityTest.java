package xxx.tamere.espressotesting.test;

import android.test.ActivityInstrumentationTestCase2;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onData;
import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.typeText;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withContentDescription;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withText;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.is;

import xxx.tamere.espressotesting.MainActivity;
import xxx.tamere.espressotesting.R;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testClick() {
        onView(withId(R.id.textView))
                .check(matches(withText(R.string.hello_world)));

        onView(withId(R.id.editText))
                .perform(typeText("Peter"));

        onView(withId(R.id.button))
                .perform(click());

        onView(withId(R.id.textView))
                .check(matches(withText("Hello, Peter!")));
    }

    public void testSpinnerInitialization() {
//        onView(withId(R.id.spinner))
//                .check(matches(withText("Eat")));
    }

    public void testSelectAnotherEntryInSpinner() {
        onView(withId(R.id.spinner))
                .perform(click());

        onData(hasToString(is("drink")))
                // .inAdapterView(withContentDescription("what are you doing now")) // withId(R.id.spinner) doesnt work here.
                .perform(click());

        onView(withId(R.id.textView))
                .check(matches(withText("I'd like to drink")));
    }
}
