
# Espresso Example

This example app shows you how to use Espresso with Gradle and Android project.


	$ ./gradlew clean
	$ ./gradlew connectedInstrumentTest
	$ open app/build/reports/instrumentTests/connected/index.html
	
	
Refresh the HTML report everytime you `./gradlew connectedInstrumentTest`.